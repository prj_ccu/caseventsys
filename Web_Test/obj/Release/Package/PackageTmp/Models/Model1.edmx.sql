
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/04/2016 14:55:40
-- Generated from EDMX file: C:\Users\dwjum\Dropbox\CAS\報名系統\Web_Test\Web_Test\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Test1];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[tbEventSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbEventSet];
GO
IF OBJECT_ID(N'[dbo].[tbGiftSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbGiftSet];
GO
IF OBJECT_ID(N'[dbo].[tbPageItemsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbPageItemsSet];
GO
IF OBJECT_ID(N'[dbo].[tbPageSelectItemSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbPageSelectItemSet];
GO
IF OBJECT_ID(N'[dbo].[tbRegPageItemsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbRegPageItemsSet];
GO
IF OBJECT_ID(N'[dbo].[tbRegPageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbRegPageSet];
GO
IF OBJECT_ID(N'[dbo].[tbRegSelectItemSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbRegSelectItemSet];
GO
IF OBJECT_ID(N'[dbo].[tbRegUpdateSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbRegUpdateSet];
GO
IF OBJECT_ID(N'[dbo].[tbTicketSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbTicketSet];
GO
IF OBJECT_ID(N'[dbo].[tbUserSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbUserSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'tbEventSet'
CREATE TABLE [dbo].[tbEventSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [EventID] int  NOT NULL,
    [EventName] nvarchar(max)  NOT NULL,
    [RegStartTime] datetime  NOT NULL,
    [RegEndTime] datetime  NOT NULL,
    [EventStartTime] datetime  NOT NULL,
    [EventEndTime] datetime  NOT NULL,
    [EventStatus] bit  NOT NULL,
    [UserNum] int  NOT NULL,
    [limitUserNum] int  NULL,
    [NowUserNum] int  NOT NULL,
    [WaitUserNum] int  NULL,
    [SetpLog] int  NOT NULL,
    [Memo] nvarchar(max)  NULL,
    [LogCreate] datetime  NOT NULL,
    [LogEdit] datetime  NOT NULL
);
GO

-- Creating table 'tbGiftSet'
CREATE TABLE [dbo].[tbGiftSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [EventID] int  NOT NULL,
    [TicketSN] int  NOT NULL,
    [GiftName] nvarchar(max)  NULL,
    [GiftCom] nvarchar(max)  NULL,
    [GiftComContactUser] nvarchar(max)  NULL,
    [GiftComContactPhone] nvarchar(max)  NULL,
    [GiftYear] nvarchar(max)  NULL,
    [GiftFormat] nvarchar(max)  NULL,
    [GiftContent] nvarchar(max)  NULL,
    [GiftSize] nvarchar(max)  NULL,
    [GiftPrice] nvarchar(max)  NULL,
    [GiftSales] nvarchar(max)  NULL,
    [GiftAnnualSales] nvarchar(max)  NULL,
    [GiftAnnualOutput] nvarchar(max)  NULL,
    [GiftGross] nvarchar(max)  NULL,
    [GiftContinueSupply] nvarchar(max)  NULL,
    [GiftMakeMode] nvarchar(max)  NULL,
    [GiftShelfMode] nvarchar(max)  NULL,
    [GiftShelfLife] nvarchar(max)  NULL,
    [GiftDistribution] nvarchar(max)  NULL,
    [WebLinks] nvarchar(max)  NULL,
    [CommunityLinks] nvarchar(max)  NULL,
    [SaleTarget] nvarchar(max)  NULL,
    [MainShellAddress] nvarchar(max)  NULL,
    [MainShellPhone] nvarchar(max)  NULL,
    [MainShellItems] nvarchar(max)  NULL,
    [GiftAbout] nvarchar(max)  NULL,
    [GiftStory] nvarchar(max)  NULL,
    [Attractions] nvarchar(max)  NULL,
    [TouristSites] nvarchar(max)  NULL,
    [ISJoin] bit  NULL
);
GO

-- Creating table 'tbPageItemsSet'
CREATE TABLE [dbo].[tbPageItemsSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [PageTableCoID] int  NOT NULL,
    [PageTableCoName] nvarchar(max)  NOT NULL,
    [PageTableCoType] nvarchar(max)  NOT NULL,
    [PageTableCoClass] nvarchar(max)  NOT NULL,
    [PageTableCoMemo] nvarchar(max)  NOT NULL,
    [PageTableCoDDate] nvarchar(max)  NULL
);
GO

-- Creating table 'tbPageSelectItemSet'
CREATE TABLE [dbo].[tbPageSelectItemSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [EventID] int  NOT NULL,
    [SelectPageID] int  NOT NULL,
    [SelectPageDDate] nvarchar(max)  NULL,
    [IsNull] bit  NULL,
    [SelectPageIDSort] nvarchar(max)  NULL
);
GO

-- Creating table 'tbRegPageItemsSet'
CREATE TABLE [dbo].[tbRegPageItemsSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [RegTableCoID] int  NOT NULL,
    [RegTableCoName] nvarchar(max)  NOT NULL,
    [RegTableCoType] nvarchar(max)  NOT NULL,
    [RegTableCoClass] nvarchar(max)  NOT NULL,
    [RegTableCoMemo] nvarchar(max)  NOT NULL,
    [RegTableCoDDate] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'tbRegPageSet'
CREATE TABLE [dbo].[tbRegPageSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [EventID] int  NOT NULL,
    [TicketSN] int  NOT NULL,
    [RUName] nvarchar(max)  NOT NULL,
    [RUJobName] nvarchar(max)  NOT NULL,
    [RUGender] bit  NOT NULL,
    [RUUserID] nvarchar(max)  NOT NULL,
    [RUBirthDate] nvarchar(max)  NOT NULL,
    [RUPhont] nvarchar(max)  NOT NULL,
    [RUEmail] nvarchar(max)  NOT NULL,
    [CompanyType] nvarchar(max)  NOT NULL,
    [CompanyName] nvarchar(max)  NOT NULL,
    [CompanyTaxID] nvarchar(max)  NOT NULL,
    [CompanyPhone] nvarchar(max)  NOT NULL,
    [CompanyFax] nvarchar(max)  NOT NULL,
    [CompanyPostalCode] nvarchar(max)  NOT NULL,
    [CompanyAddress] nvarchar(max)  NOT NULL,
    [CUser] nvarchar(max)  NOT NULL,
    [CUserPhone] nvarchar(max)  NOT NULL,
    [CUserMail] nvarchar(max)  NOT NULL,
    [Stay] nvarchar(max)  NULL,
    [IsVegetarian] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL,
    [RegUpdateID] int  NULL
);
GO

-- Creating table 'tbRegSelectItemSet'
CREATE TABLE [dbo].[tbRegSelectItemSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [EventID] int  NOT NULL,
    [SelectRegPageID] int  NOT NULL,
    [SelectRegPageIDSort] int  NULL,
    [IsNull] bit  NULL,
    [SelectRegPageDDate] nvarchar(max)  NULL
);
GO

-- Creating table 'tbRegUpdateSet'
CREATE TABLE [dbo].[tbRegUpdateSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [EventID] int  NOT NULL,
    [TicketSN] int  NOT NULL,
    [RegUpdateID] int  NOT NULL,
    [RegUpdateName] nvarchar(max)  NOT NULL,
    [RegUpdateUrl] nvarchar(max)  NOT NULL,
    [RegUpdateMemo] nvarchar(max)  NULL,
    [Note] nvarchar(max)  NULL
);
GO

-- Creating table 'tbTicketSet'
CREATE TABLE [dbo].[tbTicketSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [EventID] int  NOT NULL,
    [EventSessionID] int  NULL,
    [TicketSN] int  NOT NULL,
    [TicketStart] int  NOT NULL,
    [TicketTime] datetime  NOT NULL,
    [TicketEditTime] datetime  NULL
);
GO

-- Creating table 'tbUserSet'
CREATE TABLE [dbo].[tbUserSet] (
    [SN] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(max)  NOT NULL,
    [UserPWD] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [EventID] in table 'tbEventSet'
ALTER TABLE [dbo].[tbEventSet]
ADD CONSTRAINT [PK_tbEventSet]
    PRIMARY KEY CLUSTERED ([EventID] ASC);
GO

-- Creating primary key on [SN] in table 'tbGiftSet'
ALTER TABLE [dbo].[tbGiftSet]
ADD CONSTRAINT [PK_tbGiftSet]
    PRIMARY KEY CLUSTERED ([SN] ASC);
GO

-- Creating primary key on [SN] in table 'tbPageItemsSet'
ALTER TABLE [dbo].[tbPageItemsSet]
ADD CONSTRAINT [PK_tbPageItemsSet]
    PRIMARY KEY CLUSTERED ([SN] ASC);
GO

-- Creating primary key on [SN] in table 'tbPageSelectItemSet'
ALTER TABLE [dbo].[tbPageSelectItemSet]
ADD CONSTRAINT [PK_tbPageSelectItemSet]
    PRIMARY KEY CLUSTERED ([SN] ASC);
GO

-- Creating primary key on [SN] in table 'tbRegPageItemsSet'
ALTER TABLE [dbo].[tbRegPageItemsSet]
ADD CONSTRAINT [PK_tbRegPageItemsSet]
    PRIMARY KEY CLUSTERED ([SN] ASC);
GO

-- Creating primary key on [SN] in table 'tbRegPageSet'
ALTER TABLE [dbo].[tbRegPageSet]
ADD CONSTRAINT [PK_tbRegPageSet]
    PRIMARY KEY CLUSTERED ([SN] ASC);
GO

-- Creating primary key on [SN] in table 'tbRegSelectItemSet'
ALTER TABLE [dbo].[tbRegSelectItemSet]
ADD CONSTRAINT [PK_tbRegSelectItemSet]
    PRIMARY KEY CLUSTERED ([SN] ASC);
GO

-- Creating primary key on [SN] in table 'tbRegUpdateSet'
ALTER TABLE [dbo].[tbRegUpdateSet]
ADD CONSTRAINT [PK_tbRegUpdateSet]
    PRIMARY KEY CLUSTERED ([SN] ASC);
GO

-- Creating primary key on [SN] in table 'tbTicketSet'
ALTER TABLE [dbo].[tbTicketSet]
ADD CONSTRAINT [PK_tbTicketSet]
    PRIMARY KEY CLUSTERED ([SN] ASC);
GO

-- Creating primary key on [SN] in table 'tbUserSet'
ALTER TABLE [dbo].[tbUserSet]
ADD CONSTRAINT [PK_tbUserSet]
    PRIMARY KEY CLUSTERED ([SN] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------