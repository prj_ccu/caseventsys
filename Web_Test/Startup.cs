﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Web_Test.Startup))]
namespace Web_Test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
