﻿using System.ServiceModel.Syndication;
using System.Web.Mvc;
using System.Xml;

namespace WebApplication1.Infrastructire.ActionResults
{
    public class RssActionResult : ActionResult
    {
        private readonly SyndicationFeed feed;

        public RssActionResult()
        {
        }

        public RssActionResult(SyndicationFeed feed)
        {
            this.feed = feed;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "application/rss+xml";
            var formatter = new Rss20FeedFormatter(feed);
            using (var writer = XmlWriter.Create(context.HttpContext.Response.Output))
            {
                formatter.WriteTo(writer);
            }
        }
    }
}