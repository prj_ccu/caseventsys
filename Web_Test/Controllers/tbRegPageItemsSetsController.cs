﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Test.Models;

namespace Web_Test.Controllers
{
    public class tbRegPageItemsSetsController : Controller
    {
        private myTestEntities db = new myTestEntities();

        // GET: tbRegPageItemsSets
        public async Task<ActionResult> Index()
        {
            return View(await db.tbRegPageItemsSet.ToListAsync());
        }

        // GET: tbRegPageItemsSets/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbRegPageItemsSet tbRegPageItemsSet = await db.tbRegPageItemsSet.FindAsync(id);
            if (tbRegPageItemsSet == null)
            {
                return HttpNotFound();
            }
            return View(tbRegPageItemsSet);
        }

        // GET: tbRegPageItemsSets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tbRegPageItemsSets/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SN,RegTableCoID,RegTableCoName,RegTableCoClass,RegTableCoMemo,RegTableCoDDate")] tbRegPageItemsSet tbRegPageItemsSet)
        {
            if (ModelState.IsValid)
            {
                db.tbRegPageItemsSet.Add(tbRegPageItemsSet);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tbRegPageItemsSet);
        }

        // GET: tbRegPageItemsSets/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbRegPageItemsSet tbRegPageItemsSet = await db.tbRegPageItemsSet.FindAsync(id);
            if (tbRegPageItemsSet == null)
            {
                return HttpNotFound();
            }
            return View(tbRegPageItemsSet);
        }

        // POST: tbRegPageItemsSets/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "SN,RegTableCoID,RegTableCoName,RegTableCoClass,RegTableCoMemo,RegTableCoDDate")] tbRegPageItemsSet tbRegPageItemsSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbRegPageItemsSet).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tbRegPageItemsSet);
        }

        // GET: tbRegPageItemsSets/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbRegPageItemsSet tbRegPageItemsSet = await db.tbRegPageItemsSet.FindAsync(id);
            if (tbRegPageItemsSet == null)
            {
                return HttpNotFound();
            }
            return View(tbRegPageItemsSet);
        }

        // POST: tbRegPageItemsSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            tbRegPageItemsSet tbRegPageItemsSet = await db.tbRegPageItemsSet.FindAsync(id);
            db.tbRegPageItemsSet.Remove(tbRegPageItemsSet);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
