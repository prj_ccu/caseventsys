﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Web_Test.Models;
using System.ServiceModel.Syndication;
using WebApplication1.Infrastructire.ActionResults;

namespace Web_Test.Controllers
{
    public class RSSController : Controller
    {
        // GET: RSS
        private readonly myTestEntities db = new myTestEntities();
        public ActionResult Index()
        {
            var feed = GetFeed();
            return new RssActionResult(feed);
        }
        public SyndicationFeed GetFeed()
        {
            var hostUrl = string.Format("{0}://{1}",
    Request.Url.Scheme,
    Request.Headers["host"]);

            var feed = new SyndicationFeed(
                "CAS報名RSS",
                "CAS活動報名網站",
                new Uri(string.Concat(hostUrl, "/Rss/")));

            var items = new List<SyndicationItem>();

            var products = db.tbEventSet.OrderByDescending(x => x.EventID).Where(i=>i.EventStatus == "活動進行中");

            foreach (var p in products)
            {
                var item = new SyndicationItem(
                    string.Concat(p.EventName),
                    string.Format("活動狀態: {0}, 活動時間: {1}",
                        p.EventStatus,
                        p.EventStartTime.ToShortDateString()
                        //p.EventStartTime.ToShortTimeString()
                        ),
                    new Uri(string.Concat(hostUrl, "/CASEvent/Event/", p.EventID)),
                    "ID",
                    DateTime.Now);

                items.Add(item);
            }

            feed.Items = items;
            return feed;

        }

    }
}