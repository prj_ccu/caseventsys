﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Test.Models;

namespace Web_Test.Controllers
{
    public class tbTicketSetsController : Controller
    {
        private myTestEntities db = new myTestEntities();

        // GET: tbTicketSets
        public ActionResult Index()
        {
            return View(db.tbTicketSet.ToList());
        }

        // GET: tbTicketSets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbTicketSet tbTicketSet = db.tbTicketSet.Find(id);
            if (tbTicketSet == null)
            {
                return HttpNotFound();
            }
            return View(tbTicketSet);
        }

        // GET: tbTicketSets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tbTicketSets/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SN,EventID,EventSessionID,TicketSN,TicketStart,TicketTime,TicketEditTime")] tbTicketSet tbTicketSet)
        {
            if (ModelState.IsValid)
            {
                db.tbTicketSet.Add(tbTicketSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbTicketSet);
        }

        // GET: tbTicketSets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbTicketSet tbTicketSet = db.tbTicketSet.Find(id);
            if (tbTicketSet == null)
            {
                return HttpNotFound();
            }
            return View(tbTicketSet);
        }

        // POST: tbTicketSets/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SN,EventID,EventSessionID,TicketSN,TicketStart,TicketTime,TicketEditTime")] tbTicketSet tbTicketSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbTicketSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbTicketSet);
        }

        // GET: tbTicketSets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbTicketSet tbTicketSet = db.tbTicketSet.Find(id);
            if (tbTicketSet == null)
            {
                return HttpNotFound();
            }
            return View(tbTicketSet);
        }

        // POST: tbTicketSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbTicketSet tbTicketSet = db.tbTicketSet.Find(id);
            db.tbTicketSet.Remove(tbTicketSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
