﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using Web_Test.Models;
using Web_Test.ViewsModels;

using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Threading.Tasks;
using System.IO;


namespace Web_Test.Controllers
{
    public class WebController : Controller
    {
        myTestEntities db = new myTestEntities();
        // GET: Web
        public ActionResult Index()
        {

                return View();      
        }
        public void refreshEvent()
        {
            var EventList = db.tbEventSet.Select(i => i.EventID).ToList();
            foreach (var item in EventList)
            {
                int NUM = db.tbTicketSet.Where(i => i.EventID == item).Select(i => i.TicketSN).Count();
                (from i in db.tbEventSet where i.EventID == item select i).Single().NowUserNum = NUM;
            }
            db.SaveChanges();
        }
        public ActionResult tbEventSet_Read([DataSourceRequest]DataSourceRequest request)
        {
            return Json(db.tbEventSet.Where(i => i.EventStatus == "活動進行中").ToDataSourceResult(request));
        }
        public ActionResult Event(int? ID)
        {
            EventViewModel thisEvent = new EventViewModel();
            PageItems thisEventPageItems = new PageItems();
            List<tbPageItemsSet> tPI = db.tbPageItemsSet.ToList();
            List<tbPageSelectItemSet> tPSI = db.tbPageSelectItemSet.Where(i => i.EventID == ID).ToList();

            var items = (from a in tPSI
                         where a.EventID == ID
                         join b in tPI on a.SelectPageID equals b.PageTableCoID
                         select new PageItems
                         { SelectPageID = a.SelectPageID, SelectPageDDate = a.SelectPageDDate, IsNull = a.IsNull, SelectPageIDSort = a.SelectPageIDSort, PageTableCoID = b.PageTableCoID, PageTableCoName = b.PageTableCoName, PageTableCoType = b.PageTableCoType, PageTableCoClass = b.PageTableCoClass, PageTableCoMemo = b.PageTableCoMemo, PageTableCoDDate = b.PageTableCoDDate });
            thisEvent.basePage = items;
            thisEvent.baseEvent = db.tbEventSet.Where(i => i.EventID == ID);
            thisEvent.tbEU = db.tbEventUpdate.Where(i => i.EventID == ID);
            var RegStartTime = thisEvent.baseEvent.Select(i => i.RegStartTime).FirstOrDefault();
            var RegEndTime = thisEvent.baseEvent.Select(i => i.RegEndTime).FirstOrDefault();
            if (System.DateTime.Compare(DateTime.Now ,RegStartTime) >= 0 && DateTime.Compare(RegEndTime , DateTime.Now) <= 0)
            {
                ViewBag.RegPageBtn = "<div class=\"col-md-offset-2 col-md-10\"><h4><a class=\"btn btn-primary\" href=\"/RegPage/" + ID + "\">前往報名</a></h4></div>";
            }
            else
            {
                ViewBag.RegPageBtn = "<div class=\"col-md-offset- 2 col-md-10\"></ div > ";
            }

            ViewBag.EventID = ID;
            return View(thisEvent);
        }
        public ActionResult RegPage(int? ID)
        {
            if (ID <= 0 || ID == null)
            {
                return Redirect("index");
            }

            ViewBag.EventID = ID;
            var NowUserNum = db.tbTicketSet.Where(i => i.EventID == ID).Select(i => i.TicketSN).Count();
            var EventSet = db.tbEventSet.Where(i => i.EventID == ID).FirstOrDefault();
            
            if (EventSet.UserNum <= NowUserNum)
            {
                TempData["TicketStatus"] = "<h3>活動註冊失敗</h3><p>活動報名人數已經額滿了<p><p>如果有疑問請洽詢相關管理人員</p>";
                return View("CheackRegPage");
            }
            //if (EventSet.RegStartTime )
            //{

            //}
            
            var LinqEventName = db.tbEventSet.Where(i => i.EventID == ID).Select(i => i.EventName);
            var EventName = LinqEventName.FirstOrDefault();
            if (ID != null && EventName !=null)
            {
                ViewBag.EventName = EventName;
            }

            List<tbRegSelectItemSet> tRSI = db.tbRegSelectItemSet.Where(i => i.EventID == ID).ToList();
            List<tbRegPageItemsSet> tRI = db.tbRegPageItemsSet.ToList();

            IEnumerable<RegPageItems> RegPage = (from a in tRSI
                           where a.EventID == ID
                           join b in tRI on a.SelectRegPageID equals b.RegTableCoID
                           select new RegPageItems
                           {
                               EventID = a.EventID,
                               SelectRegPageID = a.SelectRegPageID,
                               SelectRegPageDDate = a.SelectRegPageDDate,
                               IsNull = a.IsNull,
                               SelectRegPageIDSort = a.SelectRegPageIDSort,
                               RegTableCoID = b.RegTableCoID,
                               RegTableCoName = b.RegTableCoName,
                               RegTableCoType = b.RegTableCoType,
                               RegTableCoClass = b.RegTableCoClass,
                               RegTableCoMemo = b.RegTableCoMemo,
                               RegTableCoDDate = b.RegTableCoDDate,
                               
                           });
            RegPageViewModel RPVM = new RegPageViewModel();
            RPVM.RPItems = RegPage;
            return View(RPVM);
        }

        [HttpPost]
        public ActionResult RegPage(RegPageItemsViewModel RPIVM)
        {
            if (RPIVM.EventID == 0)
            {
                return Redirect("index");
            }
            int? CheckCompanyNum = db.tbEventSet.Where(i => i.EventID == RPIVM.EventID).Select(i => i.CheckCompany).FirstOrDefault();
            int CompanyNum = 0;
            if (RPIVM.CompanyName != null)
            {
  CompanyNum = db.tbRegPageSet.Where(i => i.CompanyName == RPIVM.CompanyName).Select(i => i.CompanyName).Count();
            }
            if (CheckCompanyNum != null && CheckCompanyNum > 0)
            {
                if (CheckCompanyNum <= CompanyNum)
                {
                    TempData["TicketStatus"] = "<h3>活動註冊失敗</h3><p>本活動限制報名一個單位只能"+ CheckCompanyNum + "人報名<p><p>如果有疑問請洽詢相關管理人員</p>";
                    return View("CheackRegPage");
                }
            }
            int SN = db.tbTicketSet.Select(i => i.SN).ToList().LastOrDefault();
            int TicketSN = Convert.ToInt32(DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString()) + SN;

            CheackRegPageViewModel RegItems = new CheackRegPageViewModel();
            tbTicketSet tbT = new tbTicketSet();
            tbRegPageSet tRP = new tbRegPageSet();
            tbGiftSet tGS = new tbGiftSet();
            IEnumerable<HttpPostedFileBase> FileList = RPIVM.UpdateFile;
            RegItems.TickItem = tbT;
            RegItems.RegItem = tRP;
            RegItems.GiftItem = tGS;
            
            
            RegItems.TickItem.TicketSN = TicketSN;
            RegItems.TickItem.TicketStart = 0;
            RegItems.TickItem.TicketTime = DateTime.Now;
            RegItems.TickItem.EventID = RPIVM.EventID;

            if (FileList != null)
            {
                foreach (var item in FileList)
                {
                    tbRegUpdateSet tRUS = new tbRegUpdateSet();
                    var TicketSNFineName = Path.GetFileName(item.FileName);
                    string TicketSNFilePate = "~/File/" + TicketSN.ToString();
                    Directory.CreateDirectory(Server.MapPath(TicketSNFilePate));
                    var serviceFilePate = Path.Combine(Server.MapPath(TicketSNFilePate), TicketSNFineName);
                    tRUS.RegUpdateID = TicketSN.ToString();
                    tRUS.TicketSN = TicketSN;
                    tRUS.EventID = RPIVM.EventID;
                    tRUS.RegUpdateName = TicketSNFineName;
                    tRUS.RegUpdateUrl = TicketSNFilePate + "/" + TicketSNFineName;
                    item.SaveAs(serviceFilePate);
                    db.tbRegUpdateSet.Add(tRUS);
                }
            }

            RegItems.RegItem.EventID = RPIVM.EventID;
            RegItems.RegItem.TicketSN = TicketSN;
            RegItems.RegItem.RUName = RPIVM.RUName;
            RegItems.RegItem.RUJobName = RPIVM.RUJobName;
            RegItems.RegItem.RUGender = RPIVM.RUGender;
            RegItems.RegItem.RUUserID = RPIVM.RUUserID;
            RegItems.RegItem.RUBirthDate = RPIVM.RUBirthDate;
            RegItems.RegItem.RUPhont = RPIVM.RUPhont;
            RegItems.RegItem.RUEmail = RPIVM.RUEmail;
            RegItems.RegItem.CompanyType = RPIVM.CompanyType;
            RegItems.RegItem.CompanyName = RPIVM.CompanyName;
            RegItems.RegItem.CompanyTaxID = RPIVM.CompanyTaxID;
            RegItems.RegItem.CompanyPhone = RPIVM.CompanyPhone;
            RegItems.RegItem.CompanyFax = RPIVM.CompanyFax;
            RegItems.RegItem.CompanyPostalCode = RPIVM.CompanyPostalCode;
            RegItems.RegItem.CompanyAddress = RPIVM.CompanyAddress;
            RegItems.RegItem.CUser = RPIVM.CUser;
            RegItems.RegItem.CUserPhone = RPIVM.CUserPhone;
            RegItems.RegItem.CUserMail = RPIVM.CUserMail;
            RegItems.RegItem.Stay = RPIVM.Stay;
            RegItems.RegItem.IsVegetarian = RPIVM.IsVegetarian;
            RegItems.RegItem.Note = RPIVM.Note;
            RegItems.RegItem.RegUpdateID = TicketSN;

            RegItems.GiftItem.EventID = RPIVM.EventID;
            RegItems.GiftItem.TicketSN = TicketSN;
            RegItems.GiftItem.GiftName = RPIVM.GiftName;
            RegItems.GiftItem.GiftCom = RPIVM.GiftCom;
            RegItems.GiftItem.GiftComContactUser = RPIVM.GiftComContactUser;
            RegItems.GiftItem.GiftComContactPhone = RPIVM.GiftComContactPhone;
            RegItems.GiftItem.GiftYear = RPIVM.GiftYear;
            RegItems.GiftItem.GiftFormat = RPIVM.GiftFormat;
            RegItems.GiftItem.GiftContent = RPIVM.GiftContent;
            RegItems.GiftItem.GiftSize = RPIVM.GiftSize;
            RegItems.GiftItem.GiftPrice = RPIVM.GiftPrice;
            RegItems.GiftItem.GiftSales = RPIVM.GiftSales;
            RegItems.GiftItem.GiftAnnualSales = RPIVM.GiftAnnualSales;
            RegItems.GiftItem.GiftAnnualOutput = RPIVM.GiftAnnualOutput;
            RegItems.GiftItem.GiftGross = RPIVM.GiftGross;
            RegItems.GiftItem.GiftContinueSupply = RPIVM.GiftContinueSupply;
            RegItems.GiftItem.GiftMakeMode = RPIVM.GiftMakeMode;
            RegItems.GiftItem.GiftShelfMode = RPIVM.GiftShelfMode;
            RegItems.GiftItem.GiftShelfLife = RPIVM.GiftShelfLife;
            RegItems.GiftItem.GiftDistribution = RPIVM.GiftDistribution;
            RegItems.GiftItem.WebLinks = RPIVM.WebLinks;
            RegItems.GiftItem.CommunityLinks = RPIVM.CommunityLinks;
            RegItems.GiftItem.SaleTarget = RPIVM.SaleTarget;
            RegItems.GiftItem.MainShellAddress = RPIVM.MainShellAddress;
            RegItems.GiftItem.MainShellPhone = RPIVM.MainShellPhone;
            RegItems.GiftItem.MainShellItems = RPIVM.MainShellItems;
            RegItems.GiftItem.GiftAbout = RPIVM.GiftAbout;
            RegItems.GiftItem.GiftStory = RPIVM.GiftStory;
            RegItems.GiftItem.Attractions = RPIVM.Attractions;
            RegItems.GiftItem.TouristSites = RPIVM.TouristSites;
            RegItems.GiftItem.ISJoin = RPIVM.ISJoin;
            db.tbRegPageSet.Add(RegItems.RegItem);
            db.tbGiftSet.Add(RegItems.GiftItem);
            db.tbTicketSet.Add(RegItems.TickItem);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw;
            }
            
            refreshEvent();
            TempData["TicketSN"] = TicketSN;
            TempData["TicketStatus"] = "<h2>恭喜您註冊成功!</h2><h3> 您的註冊編號為</h3>" + TicketSN + "<br /><p> 如果有疑問請洽詢相關管理人員</p>";
            return View("CheackRegPage");
        }

        public ActionResult login()
        {
            if (TempData["LogeInStart"] != null && UserCheck(TempData["LogeInStart"] as tbUserSet) != false)
            {
                return Redirect("Manger");
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult login(tbUserSet UserID)
        {
            if (UserCheck(UserID) != false)
            {

                TempData["LogeInStart"] = UserID;
                return View("Manger");
            }
            else
            {
                TempData["Error"] = "查無帳號或密碼錯誤";
                return View();
            }

        }
        public bool UserCheck(tbUserSet UserID)
        {
            if (UserID != null)
            {
                if (db.tbUserSet.Where(i => i.UserName == UserID.UserName && i.UserPWD == UserID.UserPWD).ToList().LastOrDefault() != null)
                {
                    return (true);
                }
                else
                {
                    return (false);
                }
            }
            else
            {
                return (false);
            }
        }

       
        public ActionResult Manger()
        {
            if (TempData["LogeInStart"] != null && UserCheck(TempData["LogeInStart"] as tbUserSet) != false)
            {

                return View();
            }
            else
            {
                return Redirect("login");
            }

        }


        public ActionResult tbEventSet_Now_Read([DataSourceRequest]DataSourceRequest request)
        {
            return Json(db.tbEventSet.Where(i => i.EventStatus == "活動進行中").ToDataSourceResult(request));
        }
        public ActionResult tbEventSet_old_Read([DataSourceRequest]DataSourceRequest request)
        {
            return Json(db.tbEventSet.Where(i => i.EventStatus == "活動結束").ToDataSourceResult(request));
        }
        public ActionResult tbEventSet_noAction_Read([DataSourceRequest]DataSourceRequest request)
        {
            return Json(db.tbEventSet.Where(i => i.EventStatus == "尚未啟用").ToDataSourceResult(request));
        }
        public ActionResult RUserList(int? ID)
        {
            if (ID != null && UserCheck(TempData["LogeInStart"] as tbUserSet) != false)
            {
                var s1 = db.tbEventSet.Where(i => i.EventID == ID).Select(i => i.EventName).ToList();
                string EventName = s1.LastOrDefault();
                ViewBag.Event = ID;
                ViewBag.EventName = "活動名稱:" + EventName;
                return View();
            }
            else
            {
                return Redirect("login");
            }

        }
        public ActionResult RUserList_Read(int? ID, [DataSourceRequest]DataSourceRequest request)
        {
            var tbTS = (from a in db.tbTicketSet
                        where a.EventID == ID
                        join b in db.tbRegPageSet on a.TicketSN equals b.TicketSN
                        select new RUListItems
                        {
                            SN = a.SN,
                            EventID = a.EventID,
                            EventSessionID = a.EventID,
                            TicketSN = a.TicketSN,
                            TicketStart = a.TicketStart,
                            TicketTime = a.TicketTime,
                            TicketEditTime = a.TicketEditTime,
                            RUName = b.RUName,
                            RUJobName = b.RUJobName,
                            CUser = b.CUser,
                            CUserPhone = b.CUserPhone,
                            CUserMail = b.CUserMail,
                            CompanyName = b.CompanyName
                        });
            return Json(tbTS.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult RUExcelExport(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }
        public ActionResult CreateEvent()
        {
            int EventID_1 = db.tbEventSet.Select(i => i.EventID).ToList().LastOrDefault() + 1;
            TempData["EventID"] = EventID_1;
            return View();
        }
        public ActionResult RUserList_Read_File(int? ID, [DataSourceRequest]DataSourceRequest request)
        {


            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateEvent(CreateEventViewModel CEVM)
        {
            var LogeInStart = TempData["LogeInStart"];
            if (TempData["LogeInStart"] == null && UserCheck(TempData["LogeInStart"] as tbUserSet) == false)
            {

                return Redirect("login");
            }

            tbEventSet tES = new tbEventSet();
            int EventID_1 = Convert.ToInt16(TempData["EventID"]);
            IEnumerable<HttpPostedFileBase> FileList = CEVM.UpdateFile;

            tES.EventID = EventID_1;
            tES.EventName = CEVM.EventName;
            tES.RegStartTime = CEVM.RegStartTime;
            tES.RegEndTime = CEVM.RegEndTime;
            tES.EventStartTime = CEVM.EventStartTime;
            tES.EventEndTime = CEVM.EventEndTime;
            tES.EventStatus = "尚未啟用";
            tES.EventAddress = CEVM.EventAddress;
            tES.Eventlocation = CEVM.Eventlocation;
            tES.UserNum = CEVM.UserNum;
            tES.WaitUserNum = CEVM.WaitUserNum;
            tES.limitUserNum = CEVM.limitUserNum;
            tES.NowUserNum = CEVM.NowUserNum;
            tES.SetpLog = CEVM.SetpLog;
            tES.Memo = CEVM.Memo;
            tES.CheckCompany = CEVM.CheckCompany;
            db.tbEventSet.Add(tES);

            if (FileList != null)
            {
                foreach (var item in FileList)
                {
                    tbEventUpdate tEU = new tbEventUpdate();
                    var EventFineName = Path.GetFileName(item.FileName);
                    string TicketSNFilePate = "~/EventFiles/" + EventID_1.ToString();
                    Directory.CreateDirectory(Server.MapPath(TicketSNFilePate));
                    var serviceFilePate = Path.Combine(Server.MapPath(TicketSNFilePate), EventFineName);
                    tEU.EventUpdateID = EventID_1;
                    tEU.EventID = EventID_1;
                    tEU.EventUpdateName = EventFineName;
                    tEU.EventUpdateUrl = TicketSNFilePate + "/" + EventFineName;
                    item.SaveAs(serviceFilePate);
                    db.tbEventUpdate.Add(tEU);
                }
            }




            List<temp_tPSI> temp = new List<temp_tPSI>();

            temp.Add(new temp_tPSI { SelectPageDDate = HttpUtility.HtmlDecode(CEVM.PageReason), PageTableCoName = "PageReason" });
            temp.Add(new temp_tPSI { SelectPageDDate = HttpUtility.HtmlDecode(CEVM.PageAims), PageTableCoName = "PageAims" });
            temp.Add(new temp_tPSI { SelectPageDDate = HttpUtility.HtmlDecode(CEVM.PageSignWay), PageTableCoName = "PageSignWay" });
            temp.Add(new temp_tPSI { SelectPageDDate = HttpUtility.HtmlDecode(CEVM.PageAboutSed), PageTableCoName = "PageAboutSed" });
            var ItPSI = (from a in temp
                         join b in db.tbPageItemsSet on a.PageTableCoName equals b.PageTableCoName
                         select new tbPageSelectItemSet
                         {
                             EventID = EventID_1,
                             SelectPageID = b.PageTableCoID,
                             SelectPageDDate = a.SelectPageDDate,
                             IsNull = false,
                             SelectPageIDSort = null
                         }).ToList();
            foreach (var item in ItPSI)
            {
                db.tbPageSelectItemSet.Add(item);
            }
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw;
            }
            TempData["LogeInStart"] = LogeInStart;
            TempData["EventID"] = EventID_1;
            return Redirect("CreateRegPage");
        }
      


        public ActionResult CreateRegPage()
        {
            int EventID = Convert.ToInt32(TempData["EventID"]);
            CreateRegPageViewModel CRPVM = new CreateRegPageViewModel();
            CRPVM.RegPageItems = db.tbRegPageItemsSet.ToList();
            return View(CRPVM);
        }
        [HttpPost]
        public ActionResult CreateRegPage(List<RegPageJsonPost> postJson)
        {

            int EventID = Convert.ToInt32(TempData["EventID"]);
            
            foreach (var item in postJson)
            {
                tbRegSelectItemSet tRPSI = new tbRegSelectItemSet();
                tRPSI.EventID = EventID;
                tRPSI.SelectRegPageID = item.RegPageID;
                tRPSI.SelectRegPageDDate = item.RegPageDate;
                db.tbRegSelectItemSet.Add(tRPSI);
            }
            (from i in db.tbEventSet where i.EventID == EventID select i).Single().EventStatus = "活動進行中";
            try
            {
                db.SaveChanges();
                TempData["EventID"] = EventID;
                return Json(new { success = true });
            }
            catch (Exception ex)
            {

                return Json(new { success = false });
            }

        }
        public ActionResult CheackCRP()
        {
            int EventID = Convert.ToInt32(TempData["EventID"]);
            if (EventID != 0)
            {
                ViewBag.EventName = db.tbEventSet.Where(i => i.EventID == EventID).Select(i => i.EventName).FirstOrDefault().ToString();

            }
            else
            {
                ViewBag.EventName = 0;
            }

            return View();
        }



        public ActionResult ViewEventSet(int? ID)
        {
            editEventViewModel eEVM = new editEventViewModel();
            eEVM.baseEvent = db.tbEventSet.Where(i => i.EventID == ID).ToList();
            eEVM.basePage = (from a in db.tbPageItemsSet
                             join b in db.tbPageSelectItemSet on a.PageTableCoID equals b.SelectPageID
                             where b.EventID == ID
                             select new PITS
                             {
                                 SelectPageID = b.SelectPageID,
                                 SelectPageDDate = b.SelectPageDDate,
                                 IsNull = b.IsNull,
                                 SelectPageIDSort = b.SelectPageIDSort,
                                 PageTableCoID = a.PageTableCoID,
                                 PageTableCoName = a.PageTableCoName,
                                 PageTableCoType = a.PageTableCoType,
                                 PageTableCoClass = a.PageTableCoClass,
                                 PageTableCoMemo = a.PageTableCoMemo,
                                 PageTableCoDDate = a.PageTableCoDDate
                             }).ToList();
            //            List<RPITS> RPIRList = db.tbRegPageItemsSet.Select(i => new RPITS {
            //                cheached = false,
            //RegTableCoID = i.RegTableCoID,
            //RegTableCoName = i.RegTableCoName,
            //RegTableCoType = i.RegTableCoType,
            //RegTableCoClass = i.RegTableCoClass,
            //RegTableCoMemo = i.RegTableCoMemo,
            //RegTableCoDDate = i.RegTableCoDDate
            //            }).ToList();
            //            var list_temp = db.tbRegSelectItemSet.Where(i => i.EventID == ID).ToList();
            //            foreach (var a in RPIRList)
            //            {
            //                foreach (var b in list_temp)
            //                {
            //                    if (a.RegTableCoID == b.SelectRegPageID)
            //                    {
            //                        a.cheached = true;
            //                    }
            //                }
            //            }

            var RPIRList = (from a in db.tbRegPageItemsSet
                            join b in db.tbRegSelectItemSet on a.RegTableCoID equals b.SelectRegPageID
                            where b.EventID == ID
                            select new RPITS
                            {
                                cheached = false,
                                RegTableCoID = a.RegTableCoID,
                                RegTableCoName = a.RegTableCoName,
                                RegTableCoType = a.RegTableCoType,
                                RegTableCoClass = a.RegTableCoClass,
                                RegTableCoMemo = a.RegTableCoMemo,
                                RegTableCoDDate = a.RegTableCoDDate
                            }).ToList();
            eEVM.baseRegpage = RPIRList;

            return View(eEVM);
        }
        public ActionResult editEvent(int? ID)
        {
            editEventViewModel RPITs = new editEventViewModel();

            return View();
        }
        public ActionResult SetEvent()
        {
            return View();
        }
        public class temp_tPSI
        {
            public string SelectPageDDate { set; get; }
            public string PageTableCoName { set; get; }
        }
        public ActionResult CheackRegPage()
        {
            return View();
        }

        public void UserNumCheack()
        {
            var EventList = db.tbEventSet.Select(i => i.EventID);
            foreach (var item in EventList)
            {
                int UserNum = db.tbTicketSet.Where(i => i.EventID == item).Select(i => i.TicketSN).Count();
                //(from a in db.tbEventSet where a.EventID == item select a.NowUserNum);

            }

        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


    }
}