﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Kendo.Mvc.UI;

namespace Web_Test.Controllers
{
    public class imageFileController : EditorFileBrowserController
    {
        private readonly ThumbnailCreator thumbnailCreator;
        private readonly DirectoryBrowser directoryBrowser;
        // GET: imageFile
        private const string contentFolderRoot = "~/EventFiles/";
        private const string prettyName = "Images/";

        private const int ThumbnailHeight = 80;
        private const int ThumbnailWidth = 80;
        /// <summary>
        /// Gets the base paths from which content will be served.
        /// </summary>
        public override string ContentPath
        {
            get
            {
                return CreateUserFolder();
            }
        }
        public imageFileController()
        {
            directoryBrowser = new DirectoryBrowser();
        }

        private string CreateUserFolder()
        {
            int EventID_1 = Convert.ToInt16(TempData["EventID"]);
            
            var virtualPath = Path.Combine(contentFolderRoot, EventID_1.ToString(), prettyName);
            
            var path = Server.MapPath(virtualPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            TempData["EventID"] = EventID_1;
            return virtualPath;
        }

        private void CopyFolder(string source, string destination)
        {
            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
            }

            foreach (var file in Directory.EnumerateFiles(source))
            {
                var dest = Path.Combine(destination, Path.GetFileName(file));
                System.IO.File.Copy(file, dest);
            }

            foreach (var folder in Directory.EnumerateDirectories(source))
            {
                var dest = Path.Combine(destination, Path.GetFileName(folder));
                CopyFolder(folder, dest);
            }
        }
        //[OutputCache(Duration = 3600, VaryByParam = "path")]
        //public virtual ActionResult Thumbnail(string path)
        //{
        //    path = NormalizePath(path);

        //    if (AuthorizeThumbnail(path))
        //    {
        //        var physicalPath = Server.MapPath(path);

        //        if (System.IO.File.Exists(physicalPath))
        //        {
        //            Response.AddFileDependency(physicalPath);

        //            return CreateThumbnail(physicalPath);
        //        }
        //        else
        //        {
        //            throw new HttpException(404, "File Not Found");
        //        }
        //    }
        //    else
        //    {
        //        throw new HttpException(403, "Forbidden");
        //    }
        //}
        //public virtual bool AuthorizeThumbnail(string path)
        //{
        //    return CanAccess(path);
        //}
        //private FileContentResult CreateThumbnail(string physicalPath)
        //{
        //    using (var fileStream = System.IO.File.OpenRead(physicalPath))
        //    {
        //        var desiredSize = new ImageSize
        //        {
        //            Width = ThumbnailWidth,
        //            Height = ThumbnailHeight
        //        };

        //        const string contentType = "image/png";

        //        return File(thumbnailCreator.Create(fileStream, desiredSize, contentType), contentType);
        //    }
        //}
    }
}