﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Test.Models;

namespace Web_Test.Controllers
{
    public class tbEventSetsController : Controller
    {
        private myTestEntities db = new myTestEntities();

        // GET: tbEventSets
        public async Task<ActionResult> Index()
        {
            return View(await db.tbEventSet.ToListAsync());
        }

        // GET: tbEventSets/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbEventSet tbEventSet = await db.tbEventSet.FindAsync(id);
            if (tbEventSet == null)
            {
                return HttpNotFound();
            }
            return View(tbEventSet);
        }

        // GET: tbEventSets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tbEventSets/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SN,EventID,EventName,RegStartTime,RegEndTime,EventStartTime,EventEndTime,EventStatus,UserNum,limitUserNum,NowUserNum,WaitUserNum,SetpLog,Memo,LogCreate,LogEdit")] tbEventSet tbEventSet)
        {
            if (ModelState.IsValid)
            {
                db.tbEventSet.Add(tbEventSet);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tbEventSet);
        }

        // GET: tbEventSets/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbEventSet tbEventSet = await db.tbEventSet.FindAsync(id);
            if (tbEventSet == null)
            {
                return HttpNotFound();
            }
            return View(tbEventSet);
        }

        // POST: tbEventSets/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "SN,EventID,EventName,RegStartTime,RegEndTime,EventStartTime,EventEndTime,EventStatus,UserNum,limitUserNum,NowUserNum,WaitUserNum,SetpLog,Memo,LogCreate,LogEdit")] tbEventSet tbEventSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbEventSet).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tbEventSet);
        }

        // GET: tbEventSets/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbEventSet tbEventSet = await db.tbEventSet.FindAsync(id);
            if (tbEventSet == null)
            {
                return HttpNotFound();
            }
            return View(tbEventSet);
        }

        // POST: tbEventSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            tbEventSet tbEventSet = await db.tbEventSet.FindAsync(id);
            db.tbEventSet.Remove(tbEventSet);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
