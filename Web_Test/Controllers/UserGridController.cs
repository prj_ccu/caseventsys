﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Web_Test.Models;

namespace Web_Test.Controllers
{
    public class UserGridController : Controller
    {
        private myTestEntities db = new myTestEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult tbTicketSet_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<tbTicketSet> tbticketset = db.tbTicketSet;
            DataSourceResult result = tbticketset.ToDataSourceResult(request, tbTicketSet => new {
                SN = tbTicketSet.SN,
                EventID = tbTicketSet.EventID,
                EventSessionID = tbTicketSet.EventSessionID,
                TicketSN = tbTicketSet.TicketSN,
                TicketStart = tbTicketSet.TicketStart,
                TicketTime = tbTicketSet.TicketTime,
                TicketEditTime = tbTicketSet.TicketEditTime
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult tbTicketSet_Create([DataSourceRequest]DataSourceRequest request, tbTicketSet tbTicketSet)
        {
            if (ModelState.IsValid)
            {
                var entity = new tbTicketSet
                {
                    EventID = tbTicketSet.EventID,
                    EventSessionID = tbTicketSet.EventSessionID,
                    TicketSN = tbTicketSet.TicketSN,
                    TicketStart = tbTicketSet.TicketStart,
                    TicketTime = tbTicketSet.TicketTime,
                    TicketEditTime = tbTicketSet.TicketEditTime
                };

                db.tbTicketSet.Add(entity);
                db.SaveChanges();
                tbTicketSet.SN = entity.SN;
            }

            return Json(new[] { tbTicketSet }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult tbTicketSet_Update([DataSourceRequest]DataSourceRequest request, tbTicketSet tbTicketSet)
        {
            if (ModelState.IsValid)
            {
                var entity = new tbTicketSet
                {
                    SN = tbTicketSet.SN,
                    EventID = tbTicketSet.EventID,
                    EventSessionID = tbTicketSet.EventSessionID,
                    TicketSN = tbTicketSet.TicketSN,
                    TicketStart = tbTicketSet.TicketStart,
                    TicketTime = tbTicketSet.TicketTime,
                    TicketEditTime = tbTicketSet.TicketEditTime
                };

                db.tbTicketSet.Attach(entity);
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new[] { tbTicketSet }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult tbTicketSet_Destroy([DataSourceRequest]DataSourceRequest request, tbTicketSet tbTicketSet)
        {
            if (ModelState.IsValid)
            {
                var entity = new tbTicketSet
                {
                    SN = tbTicketSet.SN,
                    EventID = tbTicketSet.EventID,
                    EventSessionID = tbTicketSet.EventSessionID,
                    TicketSN = tbTicketSet.TicketSN,
                    TicketStart = tbTicketSet.TicketStart,
                    TicketTime = tbTicketSet.TicketTime,
                    TicketEditTime = tbTicketSet.TicketEditTime
                };

                db.tbTicketSet.Attach(entity);
                db.tbTicketSet.Remove(entity);
                db.SaveChanges();
            }

            return Json(new[] { tbTicketSet }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
