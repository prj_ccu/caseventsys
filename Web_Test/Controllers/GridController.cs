﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Web_Test.Models;

namespace Web_Test.Controllers
{
    public class GridController : Controller
    {
        private myTestEntities db = new myTestEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult tbTicketSet_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<tbTicketSet> tbticketset = db.tbTicketSet;
            DataSourceResult result = tbticketset.ToDataSourceResult(request, tbTicketSet => new {
                SN = tbTicketSet.SN,
                EventID = tbTicketSet.EventID,
                EventSessionID = tbTicketSet.EventSessionID,
                TicketSN = tbTicketSet.TicketSN,
                TicketStart = tbTicketSet.TicketStart,
                TicketTime = tbTicketSet.TicketTime,
                TicketEditTime = tbTicketSet.TicketEditTime
            });

            return Json(result);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
