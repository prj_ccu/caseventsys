﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data.Entity;
using System.Net;
using Web_Test.Models;
using Web_Test.ViewsModels;
using System.Collections;
using System.Web.Security;

namespace Web_Test.Controllers
{
    public class TestController : Controller
    {
        private myTestEntities db = new myTestEntities();
        // GET: Test
        public ActionResult Index()
        {
            
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(string Content)
        {
            string value = HttpUtility.HtmlDecode(Content);
            ViewBag.text = value;
            return View();
        }
        public ActionResult SelectList()
        {
            return View(db.tbRegPageItemsSet.AsEnumerable());

        }
        //[HttpPost]
        //public ActionResult SelectList(SlectItems sItems)
        //{
        //    //tbRegSelectItemSet IsItems = new tbRegSelectItemSet();

        //    //IsItems.SelectRegPageID = sItems.sCoID;
        //    //IsItems.SelectRegPageDDate = sItems.sCoDate;
        //    //IsItems.EventID = sItems.EventID;
        //    ViewBag.list = sItems;
        //    return View("SelectList");
        //}
        public ActionResult CheackSelect(SlectItems sItems)
        {
            List<SlectItems> lsit1 = new List<SlectItems> { sItems };
            string txt = "";
            foreach (var item in lsit1)
            {
                txt += item.ToString();
            }
            TempData["list"] = txt;
            return RedirectToAction("SelectList");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        public ActionResult Login()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(string acc, string Password)
        {

            // 登入的密碼（以 SHA1 加密）
            Password = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "SHA1");

            //這一條是去資料庫抓取輸入的帳號密碼的方法請自行實做
            //var r1 = account.GetSingleAccount(acc, Password);
            var r = db.tbUserSet.Where(i => i.UserName == acc && i.UserPWD == Password).FirstOrDefault();
            if (r == null)
            {
                TempData["Error"] = "您輸入的帳號不存在或者密碼錯誤!";
                return View();
            }

            // 登入時清空所有 Session 資料
            Session.RemoveAll();

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
              r.UserName,//你想要存放在 User.Identy.Name 的值，通常是使用者帳號
              DateTime.Now,
              DateTime.Now.AddMinutes(30),
              false,//將管理者登入的 Cookie 設定成 Session Cookie
              /*r.Rank.ToString(),*///userdata看你想存放啥
              FormsAuthentication.FormsCookiePath);

            string encTicket = FormsAuthentication.Encrypt(ticket);

            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

            return RedirectToAction("Index", "Home");

        }
        /// <summary>
        /// ///////////////
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
    public class SlectItems
    {
        public Boolean CheckStart { get; set; }
        public int SN { get; set; }
        public int RegTableCoID { get; set; }
        public string RegTableCoName { get; set; }
        public string RegTableCoType { get; set; }
        public string RegTableCoClass { get; set; }
        public string RegTableCoMemo { get; set; }
        public string RegTableCoDDate { get; set; }
    }

}