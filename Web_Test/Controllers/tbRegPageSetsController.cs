﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Test.Models;

namespace Web_Test.Controllers
{
    public class tbRegPageSetsController : Controller
    {
        private myTestEntities db = new myTestEntities();

        // GET: tbRegPageSets
        public ActionResult Index()
        {
            return View(db.tbRegPageSet.ToList());
        }

        // GET: tbRegPageSets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbRegPageSet tbRegPageSet = db.tbRegPageSet.Find(id);
            if (tbRegPageSet == null)
            {
                return HttpNotFound();
            }
            return View(tbRegPageSet);
        }

        // GET: tbRegPageSets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tbRegPageSets/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SN,EventID,TicketSN,RUName,RUJobName,RUGender,RUUserID,RUBirthDate,RUPhont,RUEmail,CompanyType,CompanyName,CompanyTaxID,CompanyPhone,CompanyFax,CompanyPostalCode,CompanyAddress,CUser,CUserPhone,CUserMail,Stay,IsVegetarian,Note,RegUpdateID")] tbRegPageSet tbRegPageSet)
        {
            if (ModelState.IsValid)
            {
                db.tbRegPageSet.Add(tbRegPageSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbRegPageSet);
        }

        // GET: tbRegPageSets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbRegPageSet tbRegPageSet = db.tbRegPageSet.Find(id);
            if (tbRegPageSet == null)
            {
                return HttpNotFound();
            }
            return View(tbRegPageSet);
        }

        // POST: tbRegPageSets/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SN,EventID,TicketSN,RUName,RUJobName,RUGender,RUUserID,RUBirthDate,RUPhont,RUEmail,CompanyType,CompanyName,CompanyTaxID,CompanyPhone,CompanyFax,CompanyPostalCode,CompanyAddress,CUser,CUserPhone,CUserMail,Stay,IsVegetarian,Note,RegUpdateID")] tbRegPageSet tbRegPageSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbRegPageSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbRegPageSet);
        }

        // GET: tbRegPageSets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbRegPageSet tbRegPageSet = db.tbRegPageSet.Find(id);
            if (tbRegPageSet == null)
            {
                return HttpNotFound();
            }
            return View(tbRegPageSet);
        }

        // POST: tbRegPageSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbRegPageSet tbRegPageSet = db.tbRegPageSet.Find(id);
            db.tbRegPageSet.Remove(tbRegPageSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
