﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Web_Test
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Web", action = "Index", id = UrlParameter.Optional }
            //).DataTokens["UseNamespaceFallback"] = false;
            //routes.MapRoute(
            //    name: "Web_Default",
            //    url: "{action}/{id}",
            //    defaults: new { controller = "Web", action = "Index", id = UrlParameter.Optional }
            //);
            routes.MapRoute(
                "RSS",                         // Route name
                "RSS/{action}/{id}", // URL with parameters
                new { controller = "RSS", action = "Index", id = "" }  // Parameter defaults
            ).DataTokens["UseNamespaceFallback"] = false;
            routes.MapRoute(
                "Test",                         // Route name
                "Test/{action}/{id}", // URL with parameters
                new { controller = "Test",action = "Index", id = "" }  // Parameter defaults
            ).DataTokens["UseNamespaceFallback"] = false;
            routes.MapRoute(
                "imageFile",                         // Route name
                "imageFile/{action}/{id}", // URL with parameters
                new { controller = "imageFile", action = "Index", id = "" }  // Parameter defaults
            ).DataTokens["UseNamespaceFallback"] = false;
            routes.MapRoute(
                "Files",                         // Route name
                "Files/{action}/{id}", // URL with parameters
                new { controller = "Files", action = "Index", id = "" }  // Parameter defaults
            ).DataTokens["UseNamespaceFallback"] = false;

            routes.MapRoute(
                    "Root",
                    "",
                    new { controller = "Web", action = "Index", id = "" }
                );
            routes.MapRoute(
                    "Web_Root",
                    "{action}/{id}",
                    new { controller = "Web", action = "Index", id = "" }
                );


        }
    }
}
