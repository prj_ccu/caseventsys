﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;

namespace Web_Test.ViewsModels
{
    public class LoginViewModel_1 : Web_Test.Models.tbUserSet
    {
        [Required]
        [Display(Name ="登入帳號")]
        
        public new string UserName { get; set; } 

        [Required]
        [DataType(DataType.Password)]
        [Display(Name ="密碼")]
        public new string UserPWD { get; set; }

    }
}