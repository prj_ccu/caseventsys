﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web_Test.Models;

namespace Web_Test.ViewsModels
{
    public class RUserListViewModel
    {
        public IEnumerable<tbRegPageSet> RegList { set; get; }
        public IEnumerable<tbGiftSet> GiftList { set; get; }
        public RUListItems RUL { set; get; }
    }
    public class RUListItems
    {
        public int SN { get; set; }
        public int EventID { get; set; }
        public Nullable<int> EventSessionID { get; set; }
        public int TicketSN { get; set; }
        public int TicketStart { get; set; }
        public System.DateTime TicketTime { get; set; }
        public Nullable<System.DateTime> TicketEditTime { get; set; }
        public string RUName { get; set; }
        public string RUJobName { get; set; }
        public string CUser { get; set; }
        public string CUserPhone { get; set; }
        public string CUserMail { get; set; }
        public string CompanyName { get; set; }
    }
}