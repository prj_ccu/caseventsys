﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web_Test.Models;
namespace Web_Test.ViewsModels
{
    public class RegPageViewModel
    {
        public IEnumerable<tbRegPageSet> RegPageItems { get; set; }
        public IEnumerable<tbGiftSet> GiftPageItems { get; set; }
        public IEnumerable<RegPageItems> RPItems { set; get; }
        public RegPageItems postRPItems { set; get; }
        public IEnumerable<HttpPostedFileBase> UpdateFile { get; set; }
        
    }
    public class RegPageItems
    {
        public int EventID { get; set; }
        public int SelectRegPageID { get; set; }
        public Nullable<int> SelectRegPageIDSort { get; set; }
        public Nullable<bool> IsNull { get; set; }
        public string SelectRegPageDDate { get; set; }
        public int RegTableCoID { get; set; }
        public string RegTableCoName { get; set; }
        public string RegTableCoType { get; set; }
        public string RegTableCoClass { get; set; }
        public string RegTableCoMemo { get; set; }
        public string RegTableCoDDate { get; set; }
    }
}