﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Web_Test.Models;

namespace Web_Test.ViewsModels
{
    public class EventViewModel
    {
        public IEnumerable<tbEventSet> baseEvent { set; get; }

        public IEnumerable<PageItems> basePage { set; get; }

        public IEnumerable<tbEventUpdate> tbEU { set; get; }

        public EventPage EP { set; get; }
    }

    public class PageItems
    {
 
        public int SelectPageID { get; set; }
        public string SelectPageDDate { get; set; }
        public bool? IsNull { get; set; }
        public int? SelectPageIDSort { get; set; }
        public int PageTableCoID { get; set; }
        public string PageTableCoName { get; set; }
        public string PageTableCoType { get; set; }
        public string PageTableCoClass { get; set; }
        public string PageTableCoMemo { get; set; }
        public string PageTableCoDDate { get; set; }
    }
    public class EventPage : CreateEventViewModel
    {


    }
}