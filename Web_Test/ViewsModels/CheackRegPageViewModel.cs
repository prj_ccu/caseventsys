﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web_Test.Models;
namespace Web_Test.ViewsModels
{
    public class CheackRegPageViewModel
    {
        public tbRegPageSet RegItem { set; get; }
        public tbTicketSet TickItem { set; get; }
        public tbGiftSet GiftItem { set; get; }
    }
}