﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web_Test.Models;

namespace Web_Test.ViewsModels
{
    public class SlectItemsViewModel
    {
        public IEnumerable<tbRegPageItemsSet> items { get; set; }
        public bool CheckStart {get; set;}
        public int SN { get; set; }
        public int RegTableCoID { get; set; }
        public string RegTableCoName { get; set; }
        public string RegTableCoType { get; set; }
        public string RegTableCoClass { get; set; }
        public string RegTableCoMemo { get; set; }
        public string RegTableCoDDate { get; set; }
    }
}