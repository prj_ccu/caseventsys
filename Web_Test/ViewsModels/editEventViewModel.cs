﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Web_Test.Models;

namespace Web_Test.ViewsModels
{
    public class editEventViewModel 
    {
        public IEnumerable<tbEventSet> baseEvent { set; get; }
        public IEnumerable<RPITS> baseRegpage { set; get; }
        public IEnumerable<PITS> basePage { set; get; }
        public tbEventSet cBaseEvent { set; get; }
        public PITS cBasePage { set; get; }
    }
    public class RPITS : RegPageItems 
    {
        public bool cheached { set; get; }
        
    }
    public class PITS : PageItems
    {
        public bool cheached { set; get; }
    }
}
