﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Web_Test.ViewsModels
{
    public class RegPageItemsViewModel
    {
        public int EventID { get; set; }
        public int TicketSN { get; set; }
        public string RUName { get; set; }
        public string RUJobName { get; set; }
        public string RUGender { get; set; }
        public string RUUserID { get; set; }
        public string RUBirthDate { get; set; }
        public string RUPhont { get; set; }
        public string RUEmail { get; set; }
        public string CompanyType { get; set; }
        public string CompanyName { get; set; }
        public string CompanyTaxID { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyFax { get; set; }
        public string CompanyPostalCode { get; set; }
        public string CompanyAddress { get; set; }
        public string CUser { get; set; }
        public string CUserPhone { get; set; }
        public string CUserMail { get; set; }
        public string Stay { get; set; }
        public string IsVegetarian { get; set; }
        public string Note { get; set; }
        public Nullable<int> RegUpdateID { get; set; }
        public string GiftName { get; set; }
        public string GiftCom { get; set; }
        public string GiftComContactUser { get; set; }
        public string GiftComContactPhone { get; set; }
        public string GiftYear { get; set; }
        public string GiftFormat { get; set; }
        public string GiftContent { get; set; }
        public string GiftSize { get; set; }
        public string GiftPrice { get; set; }
        public string GiftSales { get; set; }
        public string GiftAnnualSales { get; set; }
        public string GiftAnnualOutput { get; set; }
        public string GiftGross { get; set; }
        public string GiftContinueSupply { get; set; }
        public string GiftMakeMode { get; set; }
        public string GiftShelfMode { get; set; }
        public string GiftShelfLife { get; set; }
        public string GiftDistribution { get; set; }
        public string WebLinks { get; set; }
        public string CommunityLinks { get; set; }
        public string SaleTarget { get; set; }
        public string MainShellAddress { get; set; }
        public string MainShellPhone { get; set; }
        public string MainShellItems { get; set; }
        public string GiftAbout { get; set; }
        public string GiftStory { get; set; }
        public string Attractions { get; set; }
        public string TouristSites { get; set; }
        public Nullable<bool> ISJoin { get; set; }
        public IEnumerable<HttpPostedFileBase> UpdateFile { get; set; }
    }
}