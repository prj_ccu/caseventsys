﻿
namespace Web_Test.ViewsModels
{
    using Web_Test.Models;
    using System.Collections.Generic;

    public class SelectRegTestViewModel
    {
        public IEnumerable<tbRegPageItemsSet> Items { get; set; }
        public IEnumerable<tbPageSelectItemSet> SelectItemSet { get; set; }
    }
}