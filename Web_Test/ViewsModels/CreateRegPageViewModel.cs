﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web_Test.Models;

namespace Web_Test.ViewsModels
{
    public class CreateRegPageViewModel
    {
        public Boolean cheackBoxed { set; get; }
        public IEnumerable<tbRegPageItemsSet> RegPageItems { set; get; }

        public RegPageJsonPost RPJPI { set; get; }
    }

}

public class RegPageJsonPost
{
    public int RegPageID { set; get; }
    public string RegPageDate { set; get; }
}