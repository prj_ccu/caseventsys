﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Test.ViewsModels
{
    public class CreateEventViewModel
    {
        public int EventID { get; set; }
        public string EventName { get; set; }
        public System.DateTime RegStartTime { get; set; }
        public System.DateTime RegEndTime { get; set; }
        public System.DateTime EventStartTime { get; set; }
        public System.DateTime EventEndTime { get; set; }
        public string EventStatus { get; set; }
        public int UserNum { get; set; }
        public Nullable<int> limitUserNum { get; set; }
        public int NowUserNum { get; set; }
        public Nullable<int> WaitUserNum { get; set; }
        public int SetpLog { get; set; }
        public string Memo { get; set; }
        public System.DateTime LogCreate { get; set; }
        public System.DateTime LogEdit { get; set; }
        public string EventAddress { get; set; }
        public string Eventlocation { get; set; }

        public int CheckCompany { get; set; }

        public string PageReason { get; set; }
        public string PageAims { get; set; }
        public string PageSignWay { get; set; }
        public string PageAboutSed { get; set; }

        public IEnumerable<HttpPostedFileBase> UpdateFile { get; set; }
    }
}