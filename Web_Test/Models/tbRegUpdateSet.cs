//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Web_Test.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbRegUpdateSet
    {
        public int SN { get; set; }
        public int EventID { get; set; }
        public int TicketSN { get; set; }
        public string RegUpdateID { get; set; }
        public string RegUpdateName { get; set; }
        public string RegUpdateUrl { get; set; }
        public string RegUpdateMemo { get; set; }
        public string Note { get; set; }
    }
}
