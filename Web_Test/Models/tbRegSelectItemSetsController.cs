﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Web_Test.Models
{
    public class tbRegSelectItemSetsController : Controller
    {
        private myTestEntities db = new myTestEntities();

        // GET: tbRegSelectItemSets
        public async Task<ActionResult> Index()
        {
            return View(await db.tbRegSelectItemSet.ToListAsync());
        }

        // GET: tbRegSelectItemSets/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbRegSelectItemSet tbRegSelectItemSet = await db.tbRegSelectItemSet.FindAsync(id);
            if (tbRegSelectItemSet == null)
            {
                return HttpNotFound();
            }
            return View(tbRegSelectItemSet);
        }

        // GET: tbRegSelectItemSets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tbRegSelectItemSets/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SN,EventID,SelectRegPageID,SelectRegPageIDSort,IsNull,SelectRegPageDDate")] tbRegSelectItemSet tbRegSelectItemSet)
        {
            if (ModelState.IsValid)
            {
                db.tbRegSelectItemSet.Add(tbRegSelectItemSet);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tbRegSelectItemSet);
        }

        // GET: tbRegSelectItemSets/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbRegSelectItemSet tbRegSelectItemSet = await db.tbRegSelectItemSet.FindAsync(id);
            if (tbRegSelectItemSet == null)
            {
                return HttpNotFound();
            }
            return View(tbRegSelectItemSet);
        }

        // POST: tbRegSelectItemSets/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "SN,EventID,SelectRegPageID,SelectRegPageIDSort,IsNull,SelectRegPageDDate")] tbRegSelectItemSet tbRegSelectItemSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbRegSelectItemSet).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tbRegSelectItemSet);
        }

        // GET: tbRegSelectItemSets/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbRegSelectItemSet tbRegSelectItemSet = await db.tbRegSelectItemSet.FindAsync(id);
            if (tbRegSelectItemSet == null)
            {
                return HttpNotFound();
            }
            return View(tbRegSelectItemSet);
        }

        // POST: tbRegSelectItemSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            tbRegSelectItemSet tbRegSelectItemSet = await db.tbRegSelectItemSet.FindAsync(id);
            db.tbRegSelectItemSet.Remove(tbRegSelectItemSet);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
